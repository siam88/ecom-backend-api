
const express = require("express");
const router = express.Router();

const { addOrder, getAllOrder, getOrderById, getOrderByBrandName, putOrderById, updateOrderStatus, deleteOrderById } = require("../../controllers/order/orderController");

router.post("/post", addOrder);
router.get("/", getAllOrder);
router.get("/order-id/:id", getOrderById);
router.get("/brand/:brandName", getOrderByBrandName);
router.put("/update/:orderId", putOrderById);
router.put("/update-status/:orderId", updateOrderStatus)
router.delete("/delete/:productId", deleteOrderById);

module.exports = router;