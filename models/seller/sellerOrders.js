const mongoose = require("mongoose");
const Joi = require("joi")
Joi.objectId = require("joi-objectid")(Joi)

const {SellerInfo} = require("../customer/sellerInfo")

const orderedProductSchema = new mongoose.Schema({
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product"
    },
    productVariantId: {
        type: mongoose.Schema.Types.ObjectId,
        maxlength: 1000,
        minlength: 5,
        required: "ProductVarient is required"
    },
    quantity: {
        type: Number,
        required: "quantity is required"
    }
});
const orderSchema = new mongoose.Schema({
    sellerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    orderId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Customer",
        required: "customer id is required"
    },
    customerName: {
        type: String,
        maxlength: 200,
        minlength: 3
    },
    phone: {
        type: String,
        maxlength: 20,
        minlength: 11
    },
    orderedProducts: [
        {
            type: orderedProductSchema,
            required: true
        }
    ],
    actualCost: {
        type: Number
    },
    totalDiscount: {
        type: Number
    },
    totalCost: {
        type: Number
        // required:"total cost required"
    },
    deliveryInfo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DeliveryInfo"
    },
    status:{
        type: String, 
        enum: ["pending", "completed", "cancelled", "returned"],
        default: "pending"
    },
    createdAt: {
        type: Date,
    },
    updatedAt: {
        type: Date,
    },
    statusUpdatedAt: {
        type: Date
    }
}, {
    timestamps: true
});

orderSchema.pre("save", async function(next){
    let order = this; 
    let previousOrder = await SellerOrder.findById(order._id)
    // console.log("Order===>",order)
    // console.log("PrevOrder===>",previousOrder)

    if(previousOrder && previousOrder.status.toString() != order.status.toString()){
        let previoustStatus = previousOrder.status;
        let currentStatus = order.status; 
        let seller = await SellerInfo.findById(order.sellerId);
        seller[previoustStatus] =  seller[previoustStatus]-1 >= 0?  seller[previoustStatus]-1: 0 ;
        seller[currentStatus] +=1
        seller.save()
    }
    next()
})

const SellerOrder = mongoose.model("SellerOrder", orderSchema);

//sellerOrders.
const SellerOrdersSchema = mongoose.Schema({
    sellerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "SellerOrder"
        }
    ]

})

const SellerOrderList = mongoose.model("SellerOrderList", SellerOrdersSchema)

module.exports = { SellerOrderList, SellerOrder }


//Test
// let schema = {
//     _id: "5d9dd06222bda03ed14df4bc",
//     orders: [
//         {
//             _id: "5d9dd06222bda03ed14df4bc",
//             products: [
//                 {
//                     productId: "5d9dd06222bda03ed14df4bc",
//                     variantId: "5d9dd06222bda03ed14df4bc",
//                     quantity: 5
//                 }
//             ],
//             status: "pending",
//         }
//     ]
// }
// console.log(totalOrderValidator(schema)) 