const { SellerOrderList, SellerOrder } = require("../../models/seller/sellerOrders")

const { Product } = require("../../models/product/product")
const { ProductVariant } = require("../../models/product/productVariant")


const addOrderOnTotalSellOfSeller = async (singleOrderObjectOfSeller, response) => {
    let order = new SellerOrder(singleOrderObjectOfSeller.order)
    // console.log("order==>", order)
    await order.save()
    let sellerOrderList = await SellerOrderList.findById(singleOrderObjectOfSeller.sellerId)
    if (sellerOrderList) {
        sellerOrderList.orders.push(order._id)
    } else {
        let list = {
            _id: singleOrderObjectOfSeller.sellerId,
            sellerId: singleOrderObjectOfSeller.sellerId,
            orders: [order._id]
        }
        sellerOrderList = new SellerOrderList(list)
    }
    return sellerOrderList
}

const deleteProductFromSellerOrderList = async (object) => {
    try {
        for (let i = 0; i < object.eliminatedProducts.length; i++) {
            let eliminatedProduct = object.eliminatedProducts[i]
            let seller = await Product.findById(eliminatedProduct.productId).select("seller -_id");
            let sellerOrder = await SellerOrder.findOne({ sellerId: seller.seller, orderId: object.orderId })
            let orderedProductsAfterUpdatingOrderOfSeller = []
            for (let index = 0; index < sellerOrder.orderedProducts.length; index++) {
                product = sellerOrder.orderedProducts[index]
                if (product.productId.toString() == eliminatedProduct.productId.toString() && product.productVariantId.toString() == eliminatedProduct.productVariantId.toString()) {
                    product.isCanceled = true;
                    product.quantity = 0;
                    sellerOrder.orderedProducts[index] = product
                } else {
                    orderedProductsAfterUpdatingOrderOfSeller.push(product)
                }
            }
            const { totalCost, totalDiscount } = await processOrder(orderedProductsAfterUpdatingOrderOfSeller)
            sellerOrder.totalCost = totalCost;
            sellerOrder.totalDiscount = totalDiscount
            sellerOrder.save()
        }

    } catch (error) {
        console.log("deleteProductFromSellerOrderList", error)
    }
}

const updateSellerOrderStatusByOrderId = async (orderId, status) => {
    let orders = await SellerOrder.find({ orderId: orderId })
    for (let i = 0; i < orders.length; i++) {
        // console.log("Orders[i]=====>======", orders[i], "status: ",status) 
        orders[i].status = status;
        // console.log("Orders[i]=====>", orders[i]) 
        await orders[i].save()
    }
}

exports.getAllOrderInfomationOfSellerBySellerId = async (req, res) => {
    try {
        let id = req.params.seller_id;
        let orderInformationOfSeller = await SellerOrderList.findById(id)
            .populate({
                path: "orders",
                populate: [{
                    path: 'orderedProducts.productId'
                }]
            })
        if(!orderInformationOfSeller){
            return res.status(404).send("Order information of seller not found")
        }
        let modifiedOrderInformationOfSeller = { ...orderInformationOfSeller._doc }
        for (let orderIndex = 0; orderIndex < orderInformationOfSeller.orders.length; orderIndex++) {
            for (let productIndex = 0; productIndex < orderInformationOfSeller.orders[orderIndex].orderedProducts.length; productIndex++) {
                /*Get Variant's full information */
                let product = orderInformationOfSeller.orders[orderIndex].orderedProducts[productIndex]
                let productVariant = await ProductVariant.findById(product.productId._id.toString())
                // console.log("productVariant", productVariant)
                let variant = productVariant.variants.filter(e => e._id.toString() == product.productVariantId)[0]

                /* Spread Operation */
                let info = { ...modifiedOrderInformationOfSeller.orders[orderIndex].orderedProducts[productIndex]._doc }
                info.productVariantId = variant
                // console.log(info.productVariantId, variant)
                // return res.send(info)
                modifiedOrderInformationOfSeller.orders[orderIndex].orderedProducts[productIndex] = info
            }
        }
        return orderInformationOfSeller ? res.status(200).send(modifiedOrderInformationOfSeller) : res.status(404).send("")
    } catch (error) {
        console.log("getAllOrderInfomationOfSellerBySellerId====>", error)
        return res.status(404).send("Error", error)
    }
}

exports.getAllSellerOrderByOrderId = async(req, res)=>{
    try {
        let orders = await SellerOrder.find({ orderId: req.params.orderId })
        return orders.length? res.status(202).send(orders) : res.status(404).send("")

    } catch (error) {
        console.log("getAllSellerOrderByOrderId===>", error)
        return res.status(404).send("error ", error)
    }
}

exports.addOrderOnTotalSellOfSeller = addOrderOnTotalSellOfSeller;
exports.deleteProductFromSellerOrderList = deleteProductFromSellerOrderList;
exports.updateSellerOrderStatusByOrderId = updateSellerOrderStatusByOrderId;

const processOrder = async (orderedProducts) => {
    let valid = true;
    let status = 404;
    let errorMsg = ""
    let totalCost = 0;
    let totalDiscountAmount = 0;
    let totalDiscountPercentage = 0;
    orderedProducts = JSON.parse(JSON.stringify(orderedProducts));
    for (let i in orderedProducts) {
        // console.log(orderedProducts[i]);
        let orderedProduct = orderedProducts[i];
        // console.log("quantity==>", orderedProduct.quantity);
        let productVariant = await ProductVariant.findOne({
            "variants._id": orderedProducts[i].productVariantId
        });
        //check if the product variant does not exists.
        if (!productVariant) {
            valid = false;
            status = 404;
            errorMsg =
                "Product variant not found with Id " +
                orderedProducts[i].productVariantId;
            break;
        }
        // console.log("---=-=-=", productVariant);
        let variant = productVariant.variants.find(
            e => e._id.toString() == orderedProducts[i].productVariantId.toString()
        );

        //check for package pricing.
        if (variant.packagePricing) {
            // console.log("inside package pricing.");
            variant.packagePricing = variant.packagePricing.sort((a, b) =>
                a.piece < b.piece ? 1 : -1
            );
            for (let pp in variant.packagePricing) {
                if (variant.packagePricing[pp].piece < orderedProduct.quantity) {
                    // console.log("pp matched", variant.packagePricing[pp]);
                    //if ordered quantity matches with package price , total cost has to be calculate.
                    totalCost +=
                        variant.packagePricing[pp].price *
                        Math.floor(
                            orderedProduct.quantity / variant.packagePricing[pp].piece
                        );
                    // console.log("cost counting ", totalCost);
                    //ordered quantity has to be decreased for further calculation (of discoounts).
                    orderedProduct.quantity =
                        orderedProduct.quantity % variant.packagePricing[pp].piece;
                    // console.log("quantity counting ", orderedProduct.quantity);
                }
            }
        }
        //calculate discount amount.
        if (
            variant.discountAmount &&
            (variant.discountAmount.from <= new Date() &&
                variant.discountAmount.to >= new Date())
        ) {
            totalDiscountAmount +=
                variant.discountAmount.amount * orderedProduct.quantity;
        }

        //check for discount percentage.
        if (
            variant.discountPercentage &&
            (variant.discountPercentage.from <= new Date() &&
                variant.discountPercentage.to >= new Date())
        ) {
            totalDiscountPercentage +=
                ((variant.discountPercentage.amount * variant.price) / 100) *
                orderedProduct.quantity;
        }
        //counting total cost.
        totalCost += variant.price * orderedProduct.quantity;
        if (valid === false) break;
    }
    if (!valid) return { valid: false, errorMsg, status };
    return {
        valid: true,
        totalCost,
        totalDiscountAmount,
        totalDiscountPercentage
    };
}