const _ = require("lodash");
const { User } = require("../../models/user/user");
const { Customer } = require("../../models/customer/customer");
const { validateOrder, Order } = require("../../models/order/order");
const {
  DeliveryAddress,
  validationDeliveryAddress
} = require("../../models/customer/deliveryAddress");
const {
  SallerInfo,
  validationSallerInfo
} = require("../../models/customer/sellerInfo");
const { ProductVariant } = require("../../models/product/productVariant");
const { Product } = require("../../models/product/product")

const { addOrderOnTotalSellOfSeller, deleteProductFromSellerOrderList, updateSellerOrderStatusByOrderId } = require("../../controllers/seller/sellerOrdersController")

// const { addOrderOnTotalSellOfSeller } = require("../../controllers/seller/sellerOrdersController")

//post a order
exports.addOrder = async (req, res) => {
  // const { error } = validateOrder(req.body);
  // if (error) return res.status(400).send(error.details.map(e => e.message));
  // console.log("req-body:===>", req.body)
  const customer = Customer.findById(req.body.customertId);
  if (!customer)
    return res
      .status(404)
      .send("No customer found with this Id", req.body.customertId);

  let orderedProducts = JSON.parse(JSON.stringify(req.body.orderedProducts));
  let order = await processOrder(orderedProducts);
  // console.log("Proccessed Order=====>", order)
  if (!order.valid) return res.status(order.status).send(order.errorMsg);
  const { totalCost, totalDiscountAmount, totalDiscountPercentage } = order;

  let orderObject = {
    customerId: req.body.customerId,
    orderedProducts: req.body.orderedProducts,
    totalCost
  }
  let OrderedProduct = await new Order(orderObject)
  // console.log("OrderedProduct==========", OrderedProduct)
  let sellerOrderListArray = await selelrOrderListGenerator(req.body, OrderedProduct._id)
  for (let index = 0; index < sellerOrderListArray.length; index++) {
    let sellerOrderList = await addOrderOnTotalSellOfSeller(sellerOrderListArray[index], res)
    if (sellerOrderList) {
      await sellerOrderList.save()
    }
  }

  OrderedProduct.save()
  const update = await updateProductVariantQuantity(req.body.orderedProducts);
  if (update.accident) {
    return response.status(update.status).send(update.errorMsg);
  }
  return res.status(200).send(OrderedProduct);
};

//GET ALL order
exports.getAllOrder = async (req, res) => {
  try {
    const order = await Order.find();
    return res.status(200).send(order);
  } catch (err) {
    res.status(404).send(err);
  }
};


//get a single order By id
exports.getOrderById = async (request, response) => {
  try {
    // console.log("hit", request.params.id)
    const order = await Order.findById(request.params.id);
    return response.send(order);
  } catch (err) {
    console.log("getOrderById==>", err)
    return response.status(500).send(err);
  }
};

//GET A SINGLE order BY BRANDNAME
exports.getOrderByBrandName = async (request, response) => {
  let brandName = request.params.brandName;
  try {
    const order = await Order.find({
      brandName: brandName
    });
    return response.status(200).send(order);
  } catch (err) {
    return response.status(404).send(err);
  }
};

//UPDATE order BY ID

exports.putOrderById = async (req, res) => {
  try {
    const orderedProducts = req.body.orderedProducts
    const previousOrder = await Order.findById(req.params.orderId)

    /* Inner function for update sellerOrder, will be separated later */
    let eliminatedProductFromPreviousOrderSet = []
    //Intersection between current order and previous order
    for (let i = 0; i < previousOrder.orderedProducts.length; i++) {
      let productInCurrentOrder = false
      for (let j = 0; j < orderedProducts.length; j++) {
        if (previousOrder.orderedProducts[i].productId == orderedProducts[j].productId && previousOrder.orderedProducts[i].productVariantId == orderedProducts[j].productVariantId) {
          productInCurrentOrder = true;
          break;
        }
      }
      if (!productInCurrentOrder) {
        eliminatedProductFromPreviousOrderSet.push(previousOrder.orderedProducts[i]);
      }
    }
    // console.log(eliminatedProductFromPreviousOrderSet)
    if (eliminatedProductFromPreviousOrderSet.length) {
      let obj = {
        orderId: req.params.orderId,
        eliminatedProducts: eliminatedProductFromPreviousOrderSet
      }
      await deleteProductFromSellerOrderList(obj)
    }
    /* Inner Function End */

    previousOrder.orderedProducts = req.body.orderedProducts
    const { totalCost, totalDiscount } = await processOrder(orderedProducts)
    previousOrder.totalCost = totalCost;
    previousOrder.save()
    return res.status(200).send(previousOrder);

  } catch (err) {
    console.log("err", err)
    res.status(404).send(err);
  }
};

exports.updateOrderStatus = async (req, res) => {

  let order = await Order.findById(req.params.orderId);
  if (!order) {
    return res.status(200).send("Order Not Found")
  }
  let status = req.body.status
  if (order.status.toString() != status.toString()) {
    if (status == "cancelled") {
      order.status = status;
      order.statusUpdatedAt = new Date()
      await updateProductVariantQuantity(order.orderedProducts, true)
      await updateSellerOrderStatusByOrderId(order._id, status)
      await order.save()
    }
    if (status == "completed") {
      if (order.status != "pending") {
        return res.status(404).send("Can not complete an order which is not pending")
      }
      order.status = status;
      order.statusUpdatedAt = new Date()
      await updateSellerOrderStatusByOrderId(order._id, status)
      await order.save()
    }
    if (status == "pending") {
      if (order.status != "cancelled") {
        return res.status(404).send("Cannot proccess the request")
      }
      order.status = status;
      order.statusUpdatedAt = new Date()
      await updateSellerOrderStatusByOrderId(order._id, status)
      await order.save()
    } if (status == "returned") {
      if (order.status != "completed") {
        return res.status(404).send("Cannot proccess the request")
      }
      order.status = status;
      order.statusUpdatedAt = new Date()
      await updateSellerOrderStatusByOrderId(order._id, status)
      await order.save()
    }
  }

  return res.send(order)
}

//Delete order BY ID
exports.deleteOrderById = async (request, response) => {
  try {
    const order = await Order.findById(request.params.productId);
    let result = null;
    if (order) {
      result = await order.remove();
    }
    return response.send(result);
  } catch (error) {
    response.status(404).send(error);
  }
};

const processOrder = async (orderedProducts) => {
  let valid = true;
  let errorMsg = "";
  let status = 200;
  let totalCost = 0;
  let totalDiscountAmount = 0;
  let totalDiscountPercentage = 0;
  orderedProducts = JSON.parse(JSON.stringify(orderedProducts));
  // console.log("=========>",orderedProducts)
  for (let i in orderedProducts) {
    // console.log(orderedProducts[i]);
    let orderedProduct = orderedProducts[i];
    // console.log("quantity==>", orderedProduct.quantity);
    let productVariant = await ProductVariant.findOne({
      "variants._id": orderedProducts[i].productVariantId
    });
    //check if the product variant does not exists.
    if (!productVariant) {
      valid = false;
      status = 404;
      errorMsg =
        "Product variant not found with Id " +
        orderedProducts[i].productVariantId;
      break;
    }
    // console.log("---=-=-=", productVariant);
    let variant = productVariant.variants.find(
      e => e._id.toString() == orderedProducts[i].productVariantId.toString()
    );

    //check for insufficient quantity.
    if (variant.quantity < orderedProduct.quantity) {
      // console.log("insufficient quantity");
      valid = false;
      status = 400;
      errorMsg =
        "Product variant with Id " +
        orderedProduct.productVariantId +
        " has not enough quantity.Stock = " +
        variant.quantity;
      break;
    }
    // console.log("passed quantity");

    //check for package pricing.
    if (variant.packagePricing) {
      // console.log("inside package pricing.");
      variant.packagePricing = variant.packagePricing.sort((a, b) =>
        a.piece < b.piece ? 1 : -1
      );
      for (let pp in variant.packagePricing) {
        if (variant.packagePricing[pp].piece < orderedProduct.quantity) {
          // console.log("pp matched", variant.packagePricing[pp]);
          //if ordered quantity matches with package price , total cost has to be calculate.
          totalCost +=
            variant.packagePricing[pp].price *
            Math.floor(
              orderedProduct.quantity / variant.packagePricing[pp].piece
            );
          // console.log("cost counting ", totalCost);
          //ordered quantity has to be decreased for further calculation (of discoounts).
          orderedProduct.quantity =
            orderedProduct.quantity % variant.packagePricing[pp].piece;
          // console.log("quantity counting ", orderedProduct.quantity);
        }
      }
    }

    //calculate discount amount.
    if (
      variant.discountAmount &&
      (variant.discountAmount.from <= new Date() &&
        variant.discountAmount.to >= new Date())
    ) {
      totalDiscountAmount +=
        variant.discountAmount.amount * orderedProduct.quantity;
    }

    //check for discount percentage.
    if (
      variant.discountPercentage &&
      (variant.discountPercentage.from <= new Date() &&
        variant.discountPercentage.to >= new Date())
    ) {
      totalDiscountPercentage +=
        ((variant.discountPercentage.amount * variant.price) / 100) *
        orderedProduct.quantity;
    }
    //counting total cost.
    totalCost += variant.price * orderedProduct.quantity;
    if (valid === false) break;
  }

  if (!valid) return { valid: false, errorMsg, status };
  return {
    valid: true,
    totalCost,
    totalDiscountAmount,
    totalDiscountPercentage
  };
}


const updateProductVariantQuantity = async (orderedProducts, increase) => {
  let updateCount = 0;
  let accident = false;
  let status = 200;
  let errorMsg = "";
  for (let i in orderedProducts) {
    let orderedProduct = orderedProducts[i];
    let productVariant = await ProductVariant.findOne({
      "variants._id": orderedProduct.productVariantId
    });
    let index = 0;

    let variant = productVariant.variants.find((e, i) => {
      if (e._id.toString() == orderedProduct.productVariantId.toString()) {
        index = i;
        return e;
      }
    });

    if (!increase) {
      //check for insufficient quantity.//
      if (variant.quantity < orderedProduct.quantity) {
        status = 400;
        errorMsg =
          "Product variant with Id " +
          orderedProducts[i].productVariantId +
          " has not enough quantity.Stock = " +
          productVariant.variants[v].quantity;
        accident = true;
        break;
      }
      variant.quantity -= orderedProduct.quantity;
    } else {
      //This else block is for canceled product, increase the quantity after cancelling order//
      variant.quantity += orderedProduct.quantity;
    }
    productVariant.variants[index] = variant;

    // await productVariant.save();

    updateCount++;
  }
  if (accident) return { accident: true, status, errorMsg, updateCount };
  else return { accident: false, status: 200 };
}


const selelrOrderListGenerator = async (body, orderId) => {
  let products = body.orderedProducts;
  let productArrayOfSeller = {};
  let sellerOrderListArray = [];
  for (let i = 0; i < products.length; i++) {
    let e = products[i];
    let sellerObject = await Product.findById(e.productId).select("seller -_id");
    let sellerId = sellerObject.seller.toString();
    let obj = { productId: e.productId, productVariantId: e.productVariantId, quantity: e.quantity };
    productArrayOfSeller[sellerId] = productArrayOfSeller[sellerId] ? [...productArrayOfSeller[sellerId], obj] : [obj];
  }

  for (let i in productArrayOfSeller) {
    // console.log(productArrayOfSeller[i])
    // let stringifyProductArrayOfSeller =  JSON.parse(JSON.stringify(productArrayOfSeller[i]));
    let sellerOrder = await processOrder(productArrayOfSeller[i]);
    const { totalCost, totalDiscountAmount, totalDiscountPercentage } = sellerOrder;
    let singleOrderObjectOfSeller = {
      sellerId: i,
      order: {
        sellerId: i,
        orderId: orderId,
        customerId: body.customerId,
        orderedProducts: productArrayOfSeller[i],
        totalCost,
        totalDiscount: totalDiscountAmount
      }
    }
    // console.log(productArrayOfSeller[i])
    sellerOrderListArray.push(singleOrderObjectOfSeller)
    // console.log(singleOrderObjectOfSeller.order.orderedProducts, productArrayOfSeller[i])
    return sellerOrderListArray
  }
}


exports.processOrder = processOrder
